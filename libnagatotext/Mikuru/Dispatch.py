

FILE_HANDLER = {
    "open file": "_open_file",
    "new": "_new",
    "load": "_load",
    "save": "_save",
    "save as": "_save_as"
}
