

NOT_SAVED = \
    "<span size='large'><u>YOUR CHANGES ARE NOT SAVED !!</u></span>\n"\
    "\n"\
    "If you select Discard,\n"\
    "All your changes will be lost.\n"
