
from libnagato.ui.MainWindow import NagatoMainWindow as TFEI
from libnagato.Mikuru import HomeDirectory
from libnagatotext.eventbox.ForGrid import NagatoEventBox


class NagatoMainWindow(TFEI):

    def _yuki_n_register_closure_object(self, object_):
        self._closure = object_

    def _on_close_window(self, widget, event, user_data=None):
        if not self._closure.get_closable():
            return True
        return self._gtk_main_quit()

    def _yuki_n_new_file(self, file_name=None):
        yuki_application_name = self._enquiry("YUKI.N > data", "name")
        yuki_title = yuki_application_name if file_name is None else file_name
        self.set_title(HomeDirectory.shorten(yuki_title))
        self._raise("YUKI.N > set recent path", file_name)

    def _on_initialize(self):
        NagatoEventBox(self)
