
from libnagatotext.menu.check.WrapMode import NagatoWrapMode as MenuItem
from libnagato.menu.Sub import NagatoSubCore


class NagatoWrapMode(NagatoSubCore):

    def _initialize_child_menus(self):
        for yuki_mode in [0, 1, 2, 3]:
            MenuItem(self, yuki_mode)

    def _set_variables(self):
        self._title = "Wrap Mode"
