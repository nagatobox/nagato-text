
from libnagatotext.menu.sub.Schemes import NagatoSchems
from libnagatotext.menu.action.Font import NagatoFont
from libnagatotext.menu.action.ShowLineNumbers import NagatoShowLineNumbers
from libnagatotext.menu.sub.WrapMode import NagatoWrapMode
from libnagato.menu.SelectOpacity import NagatoSelectOpacity
from libnagato.menu.SelectBackgroundImage import NagatoSelectBackgroundImage


class AsakuraConfig(object):

    def __init__(self, parent):
        NagatoSelectOpacity(parent)
        NagatoSchems(parent)
        NagatoFont(parent)
        NagatoWrapMode(parent)
        NagatoShowLineNumbers(parent)
        NagatoSelectBackgroundImage(parent)
