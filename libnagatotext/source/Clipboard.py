
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato.object.CallDispatch import NagatoCallDispatch


class NagatoClipboard(NagatoCallDispatch):

    DISPATCH = {"cut": "_cut", "copy": "_copy", "paste": "_paste"}

    def _cut(self):
        self._buffer.cut_clipboard(self._clipboard, True)

    def _paste(self):
        self._buffer.paste_clipboard(self._clipboard, None, True)

    def _copy(self):
        self._buffer.copy_clipboard(self._clipboard)

    def __init__(self, parent):
        self._parent = parent
        self._buffer = self._enquiry("YUKI.N > buffer")
        yuki_display = Gdk.Display.get_default()
        self._clipboard = Gtk.Clipboard.get_default(yuki_display)
