
from pathlib import Path
from libnagato.object.CallDispatch import NagatoCallDispatch
from libnagato.dialog.chooser.Save import NagatoSave as Dialog


class NagatoSave(NagatoCallDispatch):

    DISPATCH = {"save": "_save", "save as": "_save_as"}

    def _save_content_to_path(self, path=None):
        if path is None:
            return False
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_text = yuki_buffer.get_property("text")
        Path(path).write_text(yuki_text)
        self._raise("YUKI.N > new path", path)
        return True

    def _save(self):
        self._raise("YUKI.N > ensure path")
        return self._save_content_to_path(self._enquiry("YUKI.N > path"))

    def _save_as(self):
        return self._save_content_to_path(Dialog.call())

    def __init__(self, parent):
        self._parent = parent
