
from libnagato.object.CallDispatch import NagatoCallDispatch
from libnagato.dialog.chooser.Load import NagatoLoad as Dialog
from libnagatotext.source.Mikuru import Path


class NagatoLoad(NagatoCallDispatch):

    DISPATCH = {"new": "_new", "load": "_load", "path": "_path"}

    def _set_text(self, text, path):
        self._raise("YUKI.N > set text to buffer", text)
        self._raise("YUKI.N > new path", path)

    def _new(self):
        self._set_text("", None)

    def _path(self, path):
        yuki_text = Path.get_contents(path)
        if yuki_text is not None:
            self._set_text(yuki_text, path)

    def _load(self):
        self._path(Dialog.call())

    def __init__(self, parent):
        self._parent = parent
        self._path(self._enquiry("YUKI.N > path"))
