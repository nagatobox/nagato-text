
from gi.repository import Gtk
from libnagato.Object import NagatoObject
from libnagatotext.source.layer.PathLayer import NagatoPathLayer


class NagatoPage(Gtk.Grid, NagatoObject):

    def _yuki_n_attach_to_grid(self, user_data):
        yuki_widget, yuki_geometries = user_data
        self.attach(yuki_widget, *yuki_geometries)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(self)
        NagatoPathLayer(self)
