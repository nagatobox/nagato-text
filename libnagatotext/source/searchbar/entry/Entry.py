
from gi.repository import Gtk
from libnagato.Object import NagatoObject

ICON_POSITION = Gtk.EntryIconPosition.PRIMARY


class NagatoEntry(Gtk.Entry, NagatoObject):

    MESSAGE = "YUKI.N > ignore"
    PLACEHOLDER = ""
    ICON_NAME = ""
    GEOMETRIES = (0, 0, 1, 1)

    def _on_activate(self, entry):
        self._raise(self.MESSAGE)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        self.set_placeholder_text(self.PLACEHOLDER)
        self.set_icon_from_icon_name(ICON_POSITION, self.ICON_NAME)
        self.connect("activate", self._on_activate)
        self.set_hexpand(True)
        self._raise("YUKI.N > attach to grid", (self, self.GEOMETRIES))

    @property
    def text(self):
        return self.get_text()
