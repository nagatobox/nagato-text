
from libnagato.Object import NagatoObject


class NagatoIter(NagatoObject):

    def set_result(self, result):
        yuki_found, yuki_start, yuki_end, _ = result
        if yuki_found:
            self._last_iters = yuki_start, yuki_end
            yuki_buffer = self._enquiry("YUKI.N > buffer")
            yuki_buffer.select_range(yuki_start, yuki_end)
            self._raise("YUKI.N > search finished", result)
        else:
            self._last_iters = None

    def __init__(self, parent):
        self._parent = parent
        self._last_iters = None

    @property
    def last(self):
        return self._last_iters
