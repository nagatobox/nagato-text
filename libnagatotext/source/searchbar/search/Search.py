
from libnagato.Object import NagatoObject
from libnagatotext.source.searchbar.search.Settings import NagatoSettings
from libnagatotext.source.searchbar.search.Context import NagatoContext


class NagatoSearch(NagatoObject):

    def search_forward(self, keyword):
        yuki_iter = self._settings.get_iter_forward(keyword)
        self._context.search_forward(yuki_iter)

    def search_backward(self, keyword):
        yuki_iter = self._settings.get_iter_backward(keyword)
        self._context.search_backward(yuki_iter)

    def replace_one(self, keyword, replacement):
        self.search_forward(keyword)
        self._context.replace_one(replacement)

    def replace_all(self, keyword, replacement):
        self.search_forward(keyword)
        self._context.replace_all(replacement)

    def __init__(self, parent):
        self._parent = parent
        self._settings = NagatoSettings(self)
        self._context = NagatoContext(self, self._settings)
