
from gi.repository.GtkSource import SearchContext
from libnagato.Object import NagatoObject
from libnagatotext.source.searchbar.iter.ForReplace import NagatoIter


class NagatoContext(NagatoObject):

    def search_forward(self, iter_):
        yuki_result = self._context.forward2(iter_)
        self._iter.set_result(yuki_result)

    def search_backward(self, iter_):
        yuki_result = self._context.backward2(iter_)
        self._iter.set_result(yuki_result)

    def replace_one(self, replacement):
        if self._iter.last is not None:
            self._context.replace(*self._iter.last, replacement, -1)

    def replace_all(self, replacement):
        self._context.replace_all(replacement, -1)

    def __init__(self, parent, settings):
        self._parent = parent
        self._iter = NagatoIter(self)
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        self._context = SearchContext.new(yuki_buffer, settings)
