
from libnagatotext.source.searchbar.button.Button import NagatoButton


class NagatoReplaceAll(NagatoButton):

    TITLE = "Replace All"
    MESSAGE = "YUKI.N > replace all"
    GEOMETRIES = (3, 1, 1, 1)
