
from gi.repository import Gtk
from libnagato.Object import NagatoObject


class NagatoButton(Gtk.Button, NagatoObject):

    TITLE = "DON'T LOOK AT ME"
    MESSAGE = "YUKI.N > ignore"
    GEOMETRIES = (0, 0, 1, 1)

    def _on_clicked(self, *args):
        self._raise(self.MESSAGE)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, self.TITLE)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > attach to grid", (self, self.GEOMETRIES))
