
from libnagatotext.source.searchbar.button.Button import NagatoButton


class NagatoReplaceOne(NagatoButton):

    TITLE = "Replace"
    MESSAGE = "YUKI.N > replace one"
    GEOMETRIES = (2, 1, 1, 1)
