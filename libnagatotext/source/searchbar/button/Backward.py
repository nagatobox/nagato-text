
from libnagatotext.source.searchbar.button.Button import NagatoButton


class NagatoBackward(NagatoButton):

    TITLE = "Back"
    MESSAGE = "YUKI.N > search backward"
    GEOMETRIES = (2, 0, 1, 1)
