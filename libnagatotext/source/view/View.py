
from gi.repository.GtkSource import View
from libnagato.Object import NagatoObject
from libnagatotext.source.view.Controls import AsakuraControls
from libnagatotext.source.view.Config import NagatoConfig
from libnagatotext.source.view.Cursor import NagatoCursor


class NagatoView(View, NagatoObject):

    def _yuki_n_file_save(self, user_data):
        self._raise("YUKI.N > file save", user_data)
        self._cursor.force_raise_signal()

    def __init__(self, parent):
        self._parent = parent
        View.__init__(self)
        self._raise("YUKI.N > buffer ready", self.get_buffer())
        self._cursor = NagatoCursor(self)
        AsakuraControls(self)
        NagatoConfig(self)
        self._parent.add(self)
