
from gi.repository.GtkSource import StyleSchemeManager
from libnagato.Object import NagatoObject


class NagatoStyleScheme(NagatoObject):

    def _set_scheme(self, scheme):
        yuki_style = self._style_scheme_manager.get_scheme(scheme)
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_buffer.set_style_scheme(yuki_style)

    def _refresh(self):
        yuki_data = "sourceview", "style_scheme"
        yuki_scheme = self._enquiry("YUKI.N > config", yuki_data)
        if yuki_scheme != "":
            self._set_scheme(yuki_scheme)

    def change_config(self):
        self._refresh()

    def __init__(self, parent):
        self._parent = parent
        self._style_scheme_manager = StyleSchemeManager.get_default()
        self._refresh()
        self._raise("YUKI.N > register config object", self)
