
from gi.repository import GtkSource
from libnagato.Object import NagatoObject


class NagatoLanguage(NagatoObject):

    def guess(self, path):
        yuki_language = self._language_manager.guess_language(path)
        if yuki_language is not None:
            yuki_buffer = self._enquiry("YUKI.N > buffer")
            yuki_buffer.set_language(yuki_language)

    def __init__(self, parent):
        self._parent = parent
        self._language_manager = GtkSource.LanguageManager.get_default()
