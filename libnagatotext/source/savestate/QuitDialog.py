
from libnagato.Object import NagatoObject
from libnagato.dialog.message.Warning import NagatoWarning

MESSAGE_QUIT = \
    "<span size='large'><u>WARNING !!</u></span>\n"\
    "\n"\
    "Do you really want to close {} ?\n"\
    "\n"


class NagatoQuitDialog(NagatoObject):

    def get_can_quit(self):
        yuki_application_name = self._enquiry("YUKI.N > data", "name")
        yuki_message = MESSAGE_QUIT.format(yuki_application_name)
        yuki_args = {"message": yuki_message, "buttons": ["Cancel", "Close"]}
        return NagatoWarning.call(**yuki_args) == 1

    def __init__(self, parent):
        self._parent = parent
