
from libnagato.Object import NagatoObject
from libnagato.dialog.message.Warning import NagatoWarning
from libnagatotext.Mikuru import Messages

MESSAGE = Messages.NOT_SAVED
BUTTONS = ["Cancel", "Discard", "Save"]
RESPONSE = (False, True, None)


class NagatoDialog(NagatoObject):

    def call(self):
        yuki_index = NagatoWarning.call(message=MESSAGE, buttons=BUTTONS)
        yuki_response = RESPONSE[yuki_index]
        if yuki_response is not None:
            return yuki_response
        return self._raise("YUKI.N > file save", ("save", None))

    def __init__(self, parent):
        self._parent = parent
