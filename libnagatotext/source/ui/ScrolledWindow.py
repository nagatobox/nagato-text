
from gi.repository import Gtk
from libnagatotext.source.view.View import NagatoView
from libnagato.Object import NagatoObject


class NagatoScrolledWindow(Gtk.ScrolledWindow, NagatoObject):

    def search_finished(self, user_data):
        yuki_finished, yuki_start, yuki_end, _ = user_data
        self._view.scroll_to_iter(yuki_start, 0, False, 0, 0)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        self.set_vexpand(True)
        self.set_hexpand(True)
        self._view = NagatoView(self)
        self._raise("YUKI.N > attach to grid", (self, (0, 0, 1, 1)))
