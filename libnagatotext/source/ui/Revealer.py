
from gi.repository import Gtk
from libnagato.Object import NagatoObject
from libnagatotext.source.searchbar.SearchBar2 import NagatoSearchBar2


class NagatoRevealer(Gtk.Revealer, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_reveal_child(False)
        self.add(NagatoSearchBar2(self))
        self._raise("YUKI.N > attach to grid", (self, (0, 2, 1, 1)))
