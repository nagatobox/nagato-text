
from gi.repository import Gtk
from libnagato.Object import NagatoObject
from libnagato.util import CssProvider
from libnagato.Ux import Unit


class NagatoStatusBar(Gtk.Revealer, NagatoObject):

    def toggle(self):
        yuki_revealed = self.get_reveal_child()
        self.set_reveal_child(not yuki_revealed)

    def set_text(self, user_data):
        yuki_lines, yuki_row, yuki_column = user_data
        yuki_data = yuki_row, yuki_lines, yuki_column
        yuki_text = "line {}/{}, column {}".format(*yuki_data)
        self._label.set_text(yuki_text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_reveal_child(False)
        self._label = Gtk.Label("")
        self._label.set_margin_top(Unit(2))
        self._label.set_size_request(-1, Unit(4))
        CssProvider.set_to_widget(self._label, "status-label")
        self.add(self._label)
        self._raise("YUKI.N > attach to grid", (self, (0, 1, 1, 1)))
