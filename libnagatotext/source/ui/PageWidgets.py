
from libnagato.Object import NagatoObject
from libnagatotext.source.ui.ScrolledWindow import NagatoScrolledWindow
from libnagatotext.source.ui.Revealer import NagatoRevealer
from libnagatotext.source.ui.StatusBar import NagatoStatusBar


class NagatoPageWidgets(NagatoObject):

    def _yuki_n_search_and_replace(self):
        yuki_revealed = not self._revealer.get_child_revealed()
        self._revealer.set_reveal_child(yuki_revealed)

    def _yuki_n_cursor_moved(self, user_data):
        self._status_bar.set_text(user_data)
        self._raise("YUKI.N > cursor moved", user_data)

    def _yuki_n_toggle_status_bar(self):
        self._status_bar.toggle()

    def _yuki_n_close_search_bar(self):
        self._revealer.set_reveal_child(False)

    def _yuki_n_search_finished(self, user_data):
        self._scrolled_window.search_finished(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._scrolled_window = NagatoScrolledWindow(self)
        self._status_bar = NagatoStatusBar(self)
        self._revealer = NagatoRevealer(self)
