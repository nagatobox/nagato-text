
from libnagato.Object import NagatoObject
from libnagatotext.source.Clipboard import NagatoClipboard
from libnagatotext.source.ui.PageWidgets import NagatoPageWidgets


class NagatoUtilLayer(NagatoObject):

    def _yuki_n_clipboard(self, command):
        self._clipboard((command, None))

    def __init__(self, parent):
        self._parent = parent
        NagatoPageWidgets(self)
        self._clipboard = NagatoClipboard(self)
