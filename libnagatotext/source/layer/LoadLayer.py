
from libnagato.Object import NagatoObject
from libnagatotext.source.layer.StyleLayer import NagatoStyleLayer
from libnagatotext.source.file.Load import NagatoLoad


class NagatoLoadLayer(NagatoObject):

    def _yuki_n_file_open(self, user_data):
        if self._enquiry("YUKI.N > is closable"):
            self._load(user_data)

    def __init__(self, parent):
        self._parent = parent
        NagatoStyleLayer(self)
        self._load = NagatoLoad(self)
