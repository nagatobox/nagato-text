
from libnagato.Object import NagatoObject
from libnagatotext.source.layer.LanguageLayer import NagatoLanguageLayer


class NagatoBufferLayer(NagatoObject):

    def _inform_buffer(self):
        return self._buffer

    def _yuki_n_buffer_ready(self, buffer_):
        self._buffer = buffer_

    def _yuki_n_set_text_to_buffer(self, text):
        self._buffer.set_text(text)

    def __init__(self, parent):
        self._parent = parent
        NagatoLanguageLayer(self)
