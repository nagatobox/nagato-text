
from libnagato.Object import NagatoObject
from libnagatotext.source.layer.SaveLayer import NagatoSaveLayer
from libnagatotext.source.manager.Language import NagatoLanguage


class NagatoLanguageLayer(NagatoObject):

    def _yuki_n_new_path(self, path=None):
        if path is not None:
            self._language_manager.guess(path)
        self._raise("YUKI.N > new path", path)

    def _yuki_n_buffer_ready(self, buffer_):
        self._raise("YUKI.N > buffer ready", buffer_)
        self._language_manager = NagatoLanguage(self)

    def __init__(self, parent):
        self._parent = parent
        NagatoSaveLayer(self)
