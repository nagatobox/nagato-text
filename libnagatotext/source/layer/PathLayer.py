
from libnagato.Object import NagatoObject
from libnagato.dialog.chooser.Save import NagatoSave
from libnagatotext.source.layer.BufferLayer import NagatoBufferLayer


class NagatoPathLayer(NagatoObject):

    def _yuki_n_new_path(self, path=None):
        self._path = path
        self._raise("YUKI.N > new file", path)

    def _yuki_n_ensure_path(self):
        if self._path is None:
            self._path = NagatoSave.call()

    def _inform_path(self):
        return self._path

    def __init__(self, parent):
        self._parent = parent
        self._path = self._enquiry("YUKI.N > args", "path")
        NagatoBufferLayer(self)
