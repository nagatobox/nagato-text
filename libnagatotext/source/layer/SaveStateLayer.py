
from libnagato.Object import NagatoObject
from libnagatotext.source.layer.LoadLayer import NagatoLoadLayer
from libnagatotext.source.SaveState import NagatoSaveState


class NagatoSaveStateLayer(NagatoObject):

    def _inform_is_closable(self):
        return self._save_state.get_closable()

    def _yuki_n_file_save(self, user_data):
        yuki_saved = self._raise("YUKI.N > file save", user_data)
        if yuki_saved:
            self._save_state.set_state_to_saved()
        return yuki_saved

    def _yuki_n_new_path(self, path=None):
        self._raise("YUKI.N > new path", path)
        self._save_state.set_state_to_saved()

    def _yuki_n_buffer_ready(self, buffer_):
        self._raise("YUKI.N > buffer ready", buffer_)
        self._save_state = NagatoSaveState(self)

    def __init__(self, parent):
        self._parent = parent
        NagatoLoadLayer(self)
