
from libnagatotext.source.searchbar.entry.Entry import NagatoEntry


class NagatoSearch(NagatoEntry):

    MESSAGE = "YUKI.N > search forward"
    PLACEHOLDER = "Text to Search"
    ICON_NAME = "edit-find-symbolic"
    GEOMETRIES = (1, 0, 1, 1)
