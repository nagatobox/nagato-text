
from libnagato.Object import NagatoObject
from libnagatotext.source.layer.UtilLayer import NagatoUtilLayer
from libnagatotext.source.manager.StyleScheme import NagatoStyleScheme


class NagatoStyleLayer(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoUtilLayer(self)
        NagatoStyleScheme(self)
