
from libnagato.menu.Item import NagatoItem
from libnagato.menu.Context import NagatoContextCore
from libnagato.menu.Separator import NagatoSeparator
from libnagatotext.source.menu.sub.RecentPaths import NagatoRecentPaths
from libnagatotext.menu.group.Config import AsakuraConfig


class NagatoContext(NagatoContextCore):

    def _on_context_menu(self, widget, event):
        if event.button == 2:
            self._raise("YUKI.N > clipboard", "paste")
        # to kill default context menu of GtkSourceView.
        if event.button == 3:
            return True

    def _add_gtk_callbacks(self):
        self._parent.connect("button-press-event", self._on_context_menu)

    def _initialize_children(self):
        NagatoItem(self, "New", "YUKI.N > file open", ("new", None))
        NagatoItem(self, "Load", "YUKI.N > file open", ("load", None))
        NagatoItem(self, "Save", "YUKI.N > file save", ("save", None))
        NagatoItem(self, "Save As", "YUKI.N > file save", ("save as", None))
        NagatoRecentPaths(self)
        self._raise("YUKI.N > set additional context menus", self)
        NagatoSeparator(self)
        NagatoItem(self, "Cut", "YUKI.N > clipboard", "cut")
        NagatoItem(self, "Copy", "YUKI.N > clipboard", "copy")
        NagatoItem(self, "Paste", "YUKI.N > clipboard", "paste")
        NagatoSeparator(self)
        NagatoItem(self, "Status Bar", "YUKI.N > toggle status bar")
        NagatoItem(self, "Search and Replace", "YUKI.N > search and replace")
        NagatoSeparator(self)
        AsakuraConfig(self)
