
from libnagato.Object import NagatoObject
from gi.repository.GtkSource import SearchSettings
from libnagatotext.source.searchbar.IterForSearch import NagatoIterForSearch


class NagatoSettings(SearchSettings, NagatoObject):

    def get_iter_forward(self, keyword):
        self.set_search_text(keyword)
        return self._iter.get_iter_forward()

    def get_iter_backward(self, keyword):
        self.set_search_text(keyword)
        return self._iter.get_iter_backward()

    def __init__(self, parent):
        self._parent = parent
        self._iter = NagatoIterForSearch(self)
        SearchSettings.__init__(self)
        self.set_wrap_around(True)
