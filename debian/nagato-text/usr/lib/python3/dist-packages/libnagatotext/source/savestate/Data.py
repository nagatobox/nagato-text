
from libnagato.Object import NagatoObject
from libnagatotext.source.savestate.QuitDialog import NagatoQuitDialog


class NagatoData(NagatoObject):

    def change_to_saved(self):
        self._saved = True

    def change_to_unsaved(self, widget):
        self._saved = False

    def get_closable(self):
        if self._saved:
            return self._dialog.get_can_quit()
            # return NagatoWarning.call(**DialogArgs.QUIT) == 1
        else:
            return self._enquiry("YUKI.N > is closable")

    def __init__(self, parent):
        self._parent = parent
        self._saved = True
        self._dialog = NagatoQuitDialog(self)
        self._raise("YUKI.N > register closure object", self)

    @property
    def saved(self):
        return self._saved
