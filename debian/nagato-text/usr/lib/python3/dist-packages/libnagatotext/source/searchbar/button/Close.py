
from libnagatotext.source.searchbar.button.Button import NagatoButton


class NagatoClose(NagatoButton):

    TITLE = "Close"
    MESSAGE = "YUKI.N > close search bar"
    GEOMETRIES = (0, 0, 1, 2)
