
from gi.repository import GLib


def _is_valid(path):
    if path is None:
        return False
    return GLib.file_test(path, GLib.FileTest.EXISTS)


def _get_content(path):
    yuki_success, yuki_content_byte = GLib.file_get_contents(path)
    if not yuki_success:
        return ""
    return yuki_content_byte.decode("utf-8")


def get_contents(path):
    if not _is_valid(path):
        return None
    return _get_content(path)
