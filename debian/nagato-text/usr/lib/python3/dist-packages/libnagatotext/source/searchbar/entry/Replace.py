
from libnagatotext.source.searchbar.entry.Entry import NagatoEntry


class NagatoReplace(NagatoEntry):

    PLACEHOLDER = "Text to Replace"
    ICON_NAME = "edit-find-replace-symbolic"
    GEOMETRIES = (1, 1, 1, 1)
