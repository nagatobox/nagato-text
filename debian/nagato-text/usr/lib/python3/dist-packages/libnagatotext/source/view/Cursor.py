
from libnagato.Object import NagatoObject


class NagatoCursor(NagatoObject):

    def _on_move_cursor(self, *args):
        yuki_offset = self._buffer.props.cursor_position
        yuki_iter = self._buffer.get_iter_at_offset(yuki_offset)
        yuki_lines = self._buffer.get_line_count()
        yuki_offset = yuki_iter.get_line_offset()
        yuki_data = yuki_lines, yuki_iter.get_line()+1, yuki_offset
        self._raise("YUKI.N > cursor moved", yuki_data)

    def force_raise_signal(self):
        self._on_move_cursor("mean machine")

    def __init__(self, parent):
        self._parent = parent
        self._buffer = self._enquiry("YUKI.N > buffer")
        self._buffer.connect("changed", self._on_move_cursor)
        parent.connect_after("move-cursor", self._on_move_cursor)
