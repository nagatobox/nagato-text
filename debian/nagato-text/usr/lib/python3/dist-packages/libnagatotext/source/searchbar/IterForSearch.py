
from libnagato.Object import NagatoObject

BACKWARD = 0
FORWARD = 1


class NagatoIterForSearch(NagatoObject):

    def _get_current_iter(self, direction):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_bounds = yuki_buffer.get_selection_bounds()
        if yuki_bounds:
            yuki_position = yuki_bounds[direction].get_offset()
        else:
            yuki_position = yuki_buffer.get_property("cursor-position")
        return yuki_buffer.get_iter_at_offset(yuki_position)

    def get_iter_backward(self):
        return self._get_current_iter(BACKWARD)

    def get_iter_forward(self):
        return self._get_current_iter(FORWARD)

    def __init__(self, parent):
        self._parent = parent
