
from libnagato.keybinds.KeyBinds import NagatoKeyBinds as TFEI
from libnagato.keybinds import Mask


MESSAGES_FOR_CTRL = {
    "n": "YUKI.N > file open",
    "o": "YUKI.N > file open",
    "s": "YUKI.N > file save",
    "f": "YUKI.N > search and replace"
}

VALUES_FOR_CTRL = {
    "n": ("new", None),
    "o": ("load", None),
    "s": ("save", None)
}


class NagatoKeyBinds(TFEI):

    def _initialize_accelerators(self):
        self._add(Mask.CTRL, MESSAGES_FOR_CTRL, VALUES_FOR_CTRL)
