
from libnagato.Object import NagatoObject
from libnagatotext.source.savestate.Data import NagatoData
from libnagatotext.source.savestate.Dialog import NagatoDialog


class NagatoSaveState(NagatoObject):

    def get_closable(self):
        if self._data.saved:
            return True
        return self._dialog.call()

    def set_state_to_saved(self):
        self._data.change_to_saved()

    def __init__(self, parent):
        self._parent = parent
        self._data = NagatoData(self)
        self._dialog = NagatoDialog(self)
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_buffer.connect("changed", self._data.change_to_unsaved)
