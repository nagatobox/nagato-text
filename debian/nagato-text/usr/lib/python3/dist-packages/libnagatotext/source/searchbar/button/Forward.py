
from libnagatotext.source.searchbar.button.Button import NagatoButton


class NagatoForward(NagatoButton):

    TITLE = "Next"
    MESSAGE = "YUKI.N > search forward"
    GEOMETRIES = (3, 0, 1, 1)
