
from libnagatotext.source.keybinds.ForSourceView import NagatoKeyBinds
from libnagatotext.source.menu.context.ForSourceView import NagatoContext


class AsakuraControls:

    def __init__(self, parent):
        NagatoKeyBinds(parent)
        NagatoContext(parent)
