
from gi.repository import Gtk
from libnagato.Object import NagatoObject
from libnagato.Ux import Unit
from libnagatotext.source.searchbar.Transaction import NagatoTransaction


class NagatoSearchBar2(Gtk.Grid, NagatoObject):

    def _yuki_n_attach_to_grid(self, user_data):
        yuki_widget, yuki_geometries = user_data
        self.attach(yuki_widget, *yuki_geometries)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(self)
        self.set_margin_top(Unit("grid-spacing"))
        self.set_row_spacing(Unit("grid-spacing")/2)
        self.set_column_spacing(Unit("grid-spacing")/4)
        NagatoTransaction(self)
