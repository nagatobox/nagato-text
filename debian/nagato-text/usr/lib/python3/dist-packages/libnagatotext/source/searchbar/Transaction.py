
from libnagato.Object import NagatoObject
from libnagatotext.source.searchbar.search.Search import NagatoSearch
from libnagatotext.source.searchbar.Widgets import NagatoWidgets


class NagatoTransaction(NagatoObject):

    def _yuki_n_search_forward(self):
        self._search.search_forward(self._widgets.search_text)

    def _yuki_n_search_backward(self):
        self._search.search_backward(self._widgets.search_text)

    def _yuki_n_replace_one(self):
        self._search.replace_one(*self._widgets.replace_keywords)

    def _yuki_n_replace_all(self):
        self._search.replace_all(*self._widgets.replace_keywords)

    def __init__(self, parent):
        self._parent = parent
        self._search = NagatoSearch(self)
        self._widgets = NagatoWidgets(self)
