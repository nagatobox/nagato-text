
from libnagato.Object import NagatoObject
from libnagatotext.source.searchbar.button.Close import NagatoClose
from libnagatotext.source.searchbar.entry.Search import NagatoSearch
from libnagatotext.source.searchbar.button.Backward import NagatoBackward
from libnagatotext.source.searchbar.button.Forward import NagatoForward
from libnagatotext.source.searchbar.button.ReplaceOne import NagatoReplaceOne
from libnagatotext.source.searchbar.button.ReplaceAll import NagatoReplaceAll
from libnagatotext.source.searchbar.entry.Replace import NagatoReplace


class NagatoWidgets(NagatoObject):

    def _initialze_widgets(self):
        NagatoClose(self)
        self._search_entry = NagatoSearch(self)
        NagatoBackward(self)
        NagatoForward(self)
        self._replace_entry = NagatoReplace(self)
        NagatoReplaceOne(self)
        NagatoReplaceAll(self)

    def __init__(self, parent):
        self._parent = parent
        self._initialze_widgets()

    @property
    def replace_keywords(self):
        return self._search_entry.text, self._replace_entry.text

    @property
    def search_text(self):
        return self._search_entry.text
