
from libnagato.Object import NagatoObject
from libnagatotext.source.layer.SaveStateLayer import NagatoSaveStateLayer
from libnagatotext.source.file.Save import NagatoSave


class NagatoSaveLayer(NagatoObject):

    def _yuki_n_file_save(self, user_data):
        return self._save(user_data)

    def __init__(self, parent):
        self._parent = parent
        NagatoSaveStateLayer(self)
        self._save = NagatoSave(self)
