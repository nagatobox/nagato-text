
from gi.repository import Gtk
from libnagato.menu.Action import NagatoActionCore
from libnagato.util import PangoFont


class NagatoFont(NagatoActionCore):

    def _refresh_font_configs(self, font_description):
        yuki_font_gtk = font_description.to_string()
        yuki_font = PangoFont.to_css(font_description)
        self._raise("YUKI.N > config", ("css", "font_gtk", yuki_font_gtk))
        self._raise("YUKI.N > config", ("css", "font", yuki_font))

    def _on_activate(self, widget):
        yuki_dialog = Gtk.FontChooserDialog(self._title, Gtk.Window())
        yuki_dialog.set_preview_text("YUKI.N > Can you see this ?")
        yuki_data = "css", "font_gtk"
        yuki_current_font = self._enquiry("YUKI.N > config", yuki_data)
        yuki_dialog.set_font(yuki_current_font)
        if Gtk.ResponseType.OK == yuki_dialog.run():
            self._refresh_font_configs(yuki_dialog.get_font_desc())
        yuki_dialog.destroy()

    def _on_map(self, widget):
        pass

    def _initialize_variables(self):
        self._title = "Select Font"
