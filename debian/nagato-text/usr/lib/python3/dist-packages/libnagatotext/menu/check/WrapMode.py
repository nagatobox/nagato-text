
from libnagatotext.menu.check.Check import NagatoCheck

TITLES = {0: "None", 1: "Char", 2: "Word", 3: "Word Char"}


class NagatoWrapMode(NagatoCheck):

    def _on_activate(self):
        yuki_data = "sourceview", "wrap_mode", self._mode
        self._raise(self._message, yuki_data)

    def _on_map(self, widget):
        yuki_current_mode = self._enquiry(self._query, self._data)
        yuki_active = (yuki_current_mode == str(self._mode))
        self._set_active_without_signal(yuki_active)

    def _initialize_variables(self, user_data=None):
        self._title = TITLES[int(user_data)]
        self._mode = user_data
        self._query = "YUKI.N > config"
        self._data = "sourceview", "wrap_mode"
        self._message = "YUKI.N > config"
